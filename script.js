window.onload = function(){
    var canvasWidth = 300;
    var canvasHeight = 300;
    var blockSize = 100;
    var ctx;
    var widthInBlocks = canvasWidth/blockSize;
    var heightInBlocks = canvasHeight/blockSize;
    var board;
    
    init();
    
    function init(){
        var canvas = document.createElement('canvas');
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        canvas.style.border = "30px solid gray";
        canvas.style.margin = "50px auto";
        canvas.style.display = "block";
        canvas.style.backgroundColor = "#ddd";
        document.body.appendChild(canvas);
        ctx = canvas.getContext('2d');
        board = new Board([["", "", ""], ["", "", ""], ["", "", ""]]);
        setBorders();
        
    }

    function setBorders(){
        ctx.font = "40px sans-serif";
        ctx.fillStyle = "gray";
        for(var i = 70; i<270; i+=37){
            ctx.fillText("|\t\t\t\t\t\t\t\t\t|", 90, i);
        }
        for(var i = 35; i<256; i+=10){
            ctx.fillText("-", i, 112);
        }
        for(var i = 35; i<256; i+=10){
            ctx.fillText("-", i, 212);
        }
    }


    function drawXO(xo, x, y){
        ctx.font = "bold 80px sans-serif";
        ctx.fillStyle = "gray";
        ctx.textAlign = "center";
        var colum = blockSize*x + 50;
        var line = blockSize*y + 80;
        ctx.fillText(xo, colum, line);
    }


    function play(x, y){

        if(board.statusOfGame === "playing"){
            board.playTrick(x, y);
            if (board.statusOfGame !== "playing"){
                board.switchXo();// pour afficher le gagnant ('X', 'O') dans gameOver()
                gameOver();
            }
        }  
    }

    
    

    function gameOver(){
        ctx.fillStyle = "#000";
        ctx.strokeStyle = "white";
        ctx.lineWidth = 5;
        var centreX = canvasWidth / 2;
        var centreY = canvasHeight / 2;

        // affichage de "Game Over"
        ctx.font = "bold 40px sans-serif";
        ctx.strokeText("Game Over", centreX, centreY - 60);
        ctx.fillText("Game Over", centreX, centreY - 60);

        ctx.font = "bold 20px sans-serif";
        if(board.statusOfGame === "victoryOrDefeat"){
            // affichage de "le 'X' a Gagner"
            ctx.strokeText("le '" + board.toPlay + "' a Gagner", centreX, centreY-30);
            ctx.fillText("le '" + board.toPlay + "' a Gagner", centreX, centreY-30);
        }
        else{
            ctx.strokeText("Draw", centreX, centreY-30);
            ctx.fillText("Draw", centreX, centreY-30);
        }

        // affichage de "Appuyer sur la touche Espace pour rejouer"
        ctx.font = "bold 14px sans-serif";
        ctx.strokeText("Appuyer sur la touche Espace pour rejouer", centreX, centreY + 30);
        ctx.fillText("Appuyer sur la touche Espace pour rejouer", centreX, centreY + 30);
    }

    function restart(){
        ctx.clearRect(0, 0, canvasWidth, canvasHeight);
        board.matrix = [["", "", ""], ["", "", ""], ["", "", ""]];
        board.statusOfGame = "playing";
        setBorders();
    }
    




    // Board ******************************************************
    function Board(matrix){
        this.matrix = matrix;
        this.statusOfGame = "playing";//"playing", "victoryOrLoose", "draw"
        this.toPlay = "X";//"X", "O"

        this.switchXo = function(){
            if(this.toPlay === "X")
                this.toPlay = "O";
            else
                this.toPlay = "X";
        }
        
        this.setStatusOfGame = function(){
            var newStatus = "playing";
            var t0 = this.matrix[0];
            var t1 = this.matrix[1];
            var t2 = this.matrix[2];

            if ((t0[0] === t0[1] && t0[0] === t0[2] && t0[0] !== "") ||
            (t1[0] === t1[1] && t1[0] === t1[2] && t1[0] !== "") ||
            (t2[0] === t2[1] && t2[0] === t2[2] && t2[0] !== "") ||
            (t0[0] === t1[0] && t0[0] === t2[0] && t0[0] !== "") ||
            (t0[1] === t1[1] && t0[1] === t2[1] && t0[1] !== "") ||
            (t0[2] === t1[2] && t0[2] === t2[2] && t0[2] !== "") ||
            (t0[0] === t1[1] && t0[0] === t2[2] && t0[0] !== "") ||
            (t0[2] === t1[1] && t0[2] === t2[0] && t0[2] !== "")){
                newStatus = "victoryOrDefeat";
            }
	        else if(t0.indexOf("") === -1 && t1.indexOf("") === -1 && t2.indexOf("") === -1){
                newStatus = "darw";
            }
            this.statusOfGame = newStatus;
        }

        this.cellIsFree = function(x, y){
            return this.matrix[x][y] === "";
        }

        this.playTrick = function(x, y){
            if (this.cellIsFree(x, y)){
                this.matrix[x][y] = this.toPlay;
                drawXO(this.toPlay, x, y);
                this.switchXo();
                this.setStatusOfGame();
            }
        }
        
    }
   
    
    document.onkeydown = function handleKeyDown(e){
        var key = e.keyCode;
        switch(key){
            case 97://1
                play(0, 2);
                break;

            case 98://2
                play(1, 2);
                break;

            case 99://3
                play(2, 2);
                break;

            case 100://4
                play(0, 1);
                break;

            case 101://5
                play(1, 1);
                break;

            case 102://6
                play(2, 1);
                break;

            case 103://7
                play(0, 0);
                break;

            case 104://8
                play(1, 0);
                break;

            case 105://9
                play(2, 0);
                break;

            case 32://sapce
                if(board.statusOfGame !== "playing"){
                    restart();
                    break;
                }

            default:
                return;
        }
    };
}